//
//  cartdet.swift
//  Restaurant
//
//  Created by TISSA Technology on 9/8/21.
//

import UIKit
import Alamofire
import SDWebImage


class cartdet: UIViewController {
    
    @IBOutlet weak var imageshowdrop: UIImageView!
    @IBOutlet weak var imageproShow: UILabel!
    @IBOutlet weak var qtyllb: UILabel!
    @IBOutlet weak var pricellb: UILabel!
    @IBOutlet weak var ingredllb: UILabel!
    @IBOutlet weak var specialinst: UITextView!
    @IBOutlet weak var costLbl: UILabel!
    
    var proQtyy = Int()
    var passunitprice = String()
    var paasprodyctimage = String()
    var specialInst = String()
    var passproductid = String()
    var passSeqid = String()
    var namepro = String()
    var ingredstr = String()
    var calculatedprice = String()
    var ingredientPrice = String()
    var passcartitemidSSt = Int()
    var passcartIID = Int()
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let url = URL(string: paasprodyctimage )
        imageshowdrop.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageshowdrop.sd_setImage(with: url) { (image, error, cache, urls) in
              if (error != nil) {
                  // Failed to load image
                self.imageshowdrop.image = UIImage(named: "noimage.png")
              } else {
                  // Successful in loading image
                self.imageshowdrop.image = image
              }
          }
        
        imageproShow.text = namepro
        
        qtyllb.text = String(proQtyy)
        costLbl.text = "Cost : $\(passunitprice)"
        specialinst.text = specialInst
        pricellb.text = "$\(calculatedprice)"
        if ingredstr == "" {
            
            ingredllb.text = ""
        }else{
            
            ingredllb.text = "Ingredients :\n\(ingredstr)"
        }
    }
    
    @IBAction func cartbackBtnClicked(_ sender: UIButton) {
        
       // self.navigationController?.popViewController(animated: false)

        self.performSegue(withIdentifier: "backcart", sender: self)

    }
    
    @IBAction func QuantityplusClicked(_ sender: UIButton) {
        
        let getqty = qtyllb.text!
        let convertqty = Int(getqty)
        proQtyy  = convertqty! + 1
        qtyllb.text = String(proQtyy)
        
        let qqty = Double(proQtyy)
       let ingredCost = Double(ingredientPrice)
        let firstprice = Double(passunitprice)
        let cal = firstprice! * qqty + ingredCost!
        pricellb.text = "$\(cal)"
    }
    
    @IBAction func QuantityminusClicked(_ sender: UIButton) {
        
        let getqty = qtyllb.text!
        let convertqty = Int(getqty)
        
        if convertqty! > 1 {
            
            proQtyy  = convertqty! - 1
            qtyllb.text = String(proQtyy)
            
            let qqty = Double(proQtyy)
           let ingredCost = Double(ingredientPrice)
            let firstprice = Double(passunitprice)
            let cal = firstprice! * qqty + ingredCost!
            pricellb.text = "$\(cal)"
            
        }else{
            
            
        }
        
    }
    
    
    
    //MARK: Webservice Call for update to cart

     
    func updateToCart() {
       
        let defaults = UserDefaults.standard
        
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        if defaults.object(forKey: "AvlbCartId") == nil {
          
            self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
           
            
        }else{
            
            var commentStr = String()
            
            if specialinst.text == nil || specialinst.text! == "" {
                commentStr = ""
            }else{
                
                commentStr = specialinst.text! as String
            }
            
           print("commentStr - \(commentStr)")
        
         let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
            let cartidStr = String(avlCartId)
            
       // let admintoken = defaults.object(forKey: "adminToken")as? String
        
        let admintoken = defaults.object(forKey: "custToken")as? String
            
            print("customer token -\(String(describing: admintoken))")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"

        let urlString = GlobalObjects.DevlopmentApi+"cart-item/\(passcartitemidSSt)/"
        
 
          
            let metadataDict = ["id":passcartitemidSSt,"product_id":passproductid, "quantity":proQtyy,"ingredient_id":0,"extra":commentStr,"sequence_id":passSeqid,"cart":passcartIID] as [String : Any]
          
            let productarr = [metadataDict]
            print("product element Dict - \(productarr)")
            
          
            
        
            let fileUrl = URL(string: urlString)

            var request = URLRequest(url: fileUrl!)
            request.httpMethod = "PUT"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(autho, forHTTPHeaderField: "Authorization")
            request.setValue(customeridStr, forHTTPHeaderField: "user_id")
            request.setValue(cartidStr, forHTTPHeaderField: "cart_id")
            request.setValue("cart-item", forHTTPHeaderField: "action")


            request.httpBody = try! JSONSerialization.data(withJSONObject: metadataDict)

            AF.request(request)
                .responseJSON { response in
                    // do whatever you want here
                    switch response.result {
                                            case .success:
                                                print(response)
                    
                                                if response.response?.statusCode == 200{
                    
                                                    self.performSegue(withIdentifier: "backcart", sender: self)
                    
                                                }else{
                    
                                                    if response.response?.statusCode == 401{
                    
                                                        ERProgressHud.sharedInstance.hide()
                                                        self.SessionAlert()
                    
                                                    }else if response.response?.statusCode == 500{
                    
                                                        ERProgressHud.sharedInstance.hide()
                    
                                                        let dict :NSDictionary = response.value! as! NSDictionary
                    
                                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                    }else{
                    
                                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                       }
                    
                                                }
                    
                                                break
                                            case .failure(let error):
                                                ERProgressHud.sharedInstance.hide()
                    
                                                print(error.localizedDescription)
                    
                                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                    
                                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                    
                                                let msgrs = "URLSessionTask failed with error: The request timed out."
                    
                                                if error.localizedDescription == msg {
                    
                                            self.showSimpleAlert(messagess:"No internet connection")
                    
                                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
                                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                    
                                                }else{
                    
                                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                                }
                    
                                                   print(error)
                                            }
            }
            
                

    }

     }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
        
        func SessionAlert() {
            let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

          
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            ERProgressHud.sharedInstance.hide()
                                            //Sign out action
                                          
                                            UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                            UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                            UserDefaults.standard.removeObject(forKey: "custToken")
                                            UserDefaults.standard.removeObject(forKey: "custId")
                                        UserDefaults.standard.removeObject(forKey: "Usertype")
                                        UserDefaults.standard.synchronize()
                                            
                                            self.performSegue(withIdentifier: "backlogin", sender: self)
            }))
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor(rgb: 0xFE9300)
        }
        
    @IBAction func updateClicked(_ sender: UIButton) {
        
    ERProgressHud.sharedInstance.show(withTitle: "Loading...")
    updateToCart()
        
    }
}
