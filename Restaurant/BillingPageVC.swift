//
//  BillingPageVC.swift
//  Restaurant
//
//  Created by TISSA Technology on 11/24/20.
//

import UIKit
import Alamofire

class BillingPageVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var noteLbl: UILabel!
    @IBOutlet weak var notefullView: UIView!
    @IBOutlet weak var exixtingaddBtn: UIButton!
    @IBOutlet weak var existingTitleLbl: UILabel!
    @IBOutlet weak var exixtingAddLbl: UILabel!
    @IBOutlet weak var notehalfView: UIView!
    @IBOutlet weak var fullnameLbl: UILabel!
    @IBOutlet weak var fullnameView: UIView!
    @IBOutlet weak var companynameLbl: UILabel!
    @IBOutlet weak var companyLblView: UIView!
    @IBOutlet weak var addLineLbl: UILabel!
    @IBOutlet weak var addLineView: UIView!
    @IBOutlet weak var housenumLbl: UILabel!
    @IBOutlet weak var housenumView: UIView!
    @IBOutlet weak var cityLbl: UILabel!
    @IBOutlet weak var cityView: UIView!
    @IBOutlet weak var zipLbl: UILabel!
    @IBOutlet weak var zipView: UIView!
    @IBOutlet weak var countrylbl: UILabel!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var stateLbl: UILabel!
    @IBOutlet weak var stateView: UIView!
    @IBOutlet weak var pagescrollView: UIScrollView!
    @IBOutlet weak var fullnameTopCont: NSLayoutConstraint!
    @IBOutlet weak var fullnameTf: UITextField!
    @IBOutlet weak var companynameTf: UITextField!
    @IBOutlet weak var addresslineTf: UITextField!
    @IBOutlet weak var housenumberTf: UITextField!
    @IBOutlet weak var cityTf: UITextField!
    @IBOutlet weak var zipTf: UITextField!
    @IBOutlet weak var countryTf: UITextField!
    @IBOutlet weak var stateTf: UITextField!
    @IBOutlet weak var billingNextBtn: UIButton!
    @IBOutlet weak var blurview: UIView!
    @IBOutlet weak var caBtn: UIButton!
    @IBOutlet weak var txBtn: UIButton!
    @IBOutlet weak var stateTable: UITableView!

    
    
    
    var nameadd = NSString()
    var addressadd = NSString()
    var hounseadd = NSString()
    var cityadd = NSString()
    var stateadd = NSString()
    var countryadd = NSString()
    var sipadd = NSString()
    var companynameadd = NSString()
    var billId = NSString()
    var passbillingdata = NSDictionary()
    var billingaddressData = NSArray()
    var billing = NSString()
    var useexistadd = NSString()
    var shippingMethod = NSArray()
    var StateDict = NSMutableArray()
    var PageCount = Int()
    var isLoading = false
    
    func loadData() {
        isLoading = false
       // ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        loadState()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        blurview.isHidden = true
        billingNextBtn.layer.cornerRadius = 6
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        PageCount = 1
//        StateDict = []
//        loadState()
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        setup()
        loadshippingMethod()
        loadBillingAddress()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        fullnameTf.text = ""
        companynameTf.text = ""
         addresslineTf.text = ""
         housenumberTf.text = ""
         cityTf.text = ""
         zipTf.text = ""
        stateTf.text = ""
        
        blurview.isHidden = true
        billingNextBtn.layer.cornerRadius = 6
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        PageCount = 1
//        StateDict = []
//        loadState()
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        setup()
        loadshippingMethod()
        loadBillingAddress()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }
    
    func setup() {
      
        notehalfView.layer.borderWidth = 1
        notehalfView.layer.cornerRadius = 6
        notehalfView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        fullnameView.layer.borderWidth = 1
        fullnameView.layer.cornerRadius = 6
        fullnameView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        companyLblView.layer.borderWidth = 1
        companyLblView.layer.cornerRadius = 6
        companyLblView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        addLineView.layer.borderWidth = 1
        addLineView.layer.cornerRadius = 6
        addLineView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        housenumView.layer.borderWidth = 1
        housenumView.layer.cornerRadius = 6
        housenumView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        cityView.layer.borderWidth = 1
        cityView.layer.cornerRadius = 6
        cityView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        zipView.layer.borderWidth = 1
        zipView.layer.cornerRadius = 6
        zipView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        countryView.layer.borderWidth = 1
        countryView.layer.cornerRadius = 6
        countryView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        stateView.layer.borderWidth = 1
        stateView.layer.cornerRadius = 6
        stateView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
    }
    

    func loadBillingAddress(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalObjects.DevlopmentApi + "billing/?customer_id=\(customerId)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("billing address - \(dict1)")
                                  
                                self.billingaddressData = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    
                                    
                                    if self.billingaddressData.count == 0 {
                                     
                                        ERProgressHud.sharedInstance.hide()
                                      self.billing = "no"
                                        self.useexistadd = "no"
                                      
                                        if billing == "no" {
                                            
                                            self.notefullView.isHidden = true
                                            self.notehalfView.isHidden = true
                                            self.existingTitleLbl.isHidden = true
                                            self.exixtingaddBtn.isHidden = true
                                            self.exixtingAddLbl.isHidden = true
                                            self.noteLbl.isHidden = true
                                            self.fullnameTopCont.constant = 12
                                            
                                        }else{
                                            
                                            self.notefullView.isHidden = false
                                            self.notehalfView.isHidden = false
                                            self.existingTitleLbl.isHidden = false
                                            self.exixtingaddBtn.isHidden = false
                                            self.exixtingAddLbl.isHidden = false
                                            self.noteLbl.isHidden = false
                                            self.fullnameTopCont.constant = 169
                                            
                                        }
                                   
                               }else{
                                  
//                                  let imagehome = UIImage(named: "dot.png") as UIImage?
//                                  self.homepicBtn.setBackgroundImage(imagehome, for: .normal)
                                     
                                  self.billing = "yes"
                                self.useexistadd = "yes"
                                  
                                  let firstobj:NSDictionary  = self.billingaddressData.object(at: 0) as! NSDictionary
                                  
                                self.passbillingdata = firstobj
                                
                                self.nameadd = firstobj["name"]as! NSString
                                self.addressadd = firstobj["address"]as! NSString
                                self.hounseadd = firstobj["house_number"]as! NSString
                                self.cityadd = firstobj["city"]as! NSString
                                self.stateadd = firstobj["state"]as! NSString
                                self.countryadd = firstobj["country"]as! NSString
                                self.sipadd = firstobj["zip"]as! NSString
                                  
                                let shipId = firstobj["id"] as! Int

                                 self.billId = String(shipId) as NSString
                                
                                  
                                self.exixtingAddLbl.text = "\(self.nameadd)\n"+"\(self.addressadd), "+"\(self.hounseadd)\n"+"\(self.cityadd), "+"\(self.stateadd), "+"\(self.countryadd),\n"+"\(self.sipadd)."
                                  
                                  
                                  let image = UIImage(named:"right.png") as UIImage?
                                  self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
                                  self.exixtingaddBtn.tag = 1
                                  
                                  self.fullnameTf.isUserInteractionEnabled = false
                                  self.companynameTf.isUserInteractionEnabled = false
                                  self.addresslineTf.isUserInteractionEnabled = false
                                  self.housenumberTf.isUserInteractionEnabled = false
                                  self.cityTf.isUserInteractionEnabled = false
                                  self.zipTf.isUserInteractionEnabled = false
                                  self.countryTf.isUserInteractionEnabled = false
                                  self.stateTf.isUserInteractionEnabled = false
                      
                                ERProgressHud.sharedInstance.hide()

                               }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
    
    func loadState(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
      //  let country = "United States of America"
       // let trimmed = country.trimmingCharacters(in: .whitespacesAndNewlines)
     //   let trimmed = country.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        let urlString = GlobalObjects.DevlopmentApi + "tax/?country_code=US&page=\(PageCount)"
        
      //  let trimmed = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict1 :NSDictionary = response.value! as! NSDictionary
                                       
                                       print("billing address - \(dict1)")
                                     
                                    
                                    
                                    let arraysh: NSArray  = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    if arraysh.count == 0 {
                                       
                                        self.StateDict = []
                                        stateTable.reloadData()
                                    }else{
                                        
                                        let sortedArray = (arraysh as NSArray).sortedArray(using: [NSSortDescriptor(key: "state", ascending: true)]) as! [[String:AnyObject]]

                                        
                                     //   self.StateDict = arraysh
                                        self.StateDict.addObjects(from:sortedArray as [Any])
                                        stateTable.reloadData()
                                    }
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 404{
                                        
                                       
                                        
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                        
                                    }
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                                    self.showSimpleAlert(messagess:"Slow Internet Detected")
                                            
                                        }else{
                                        
                                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                        }

                                           print(error)
                                    }
            }
            
      
            
        }
    
    
    //MARK: - Table View Delegates And Datasource
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
       
        let lastData = self.StateDict.count - 1
        if !isLoading && indexPath.row == lastData {
            PageCount += 1
            self.loadData()
        }
    }
    
  // number of rows in table view
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return StateDict.count
         

    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "MyCell", for: indexPath as IndexPath)


         let dictObj = self.StateDict[indexPath.row] as! NSDictionary

        cell.textLabel!.text = dictObj["state"] as? String

        cell.textLabel?.textAlignment = .center

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
          
            return 45
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
        let dictObj = self.StateDict[indexPath.row] as! NSDictionary

        blurview.isHidden = true
        stateTf.text = dictObj["state"] as? String
    }
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: messagess, message: nil,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        //Sign out action
                                        ERProgressHud.sharedInstance.hide()
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
 
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
    
    
    
    @IBAction func billingpagebackClicked(_ sender: Any) {
        
        self.navigationController?.popViewController(animated: false)
        
    }
    
    
    @IBAction func existingaddBtnClicked(_ sender: Any) {
        
        if exixtingaddBtn.tag == 0 {
           
            
            let image = UIImage(named: "right.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
            
            self.fullnameTf.isUserInteractionEnabled = false
            self.companynameTf.isUserInteractionEnabled = false
            self.addresslineTf.isUserInteractionEnabled = false
            self.housenumberTf.isUserInteractionEnabled = false
            self.cityTf.isUserInteractionEnabled = false
            self.zipTf.isUserInteractionEnabled = false
            self.countryTf.isUserInteractionEnabled = false
            self.stateTf.isUserInteractionEnabled = false
            
            exixtingaddBtn.tag = 1
            self.useexistadd = "yes"
            
        }else{
          
            let image = UIImage(named: "blank-square.png") as UIImage?
            self.exixtingaddBtn.setBackgroundImage(image, for: .normal)
            
            self.fullnameTf.isUserInteractionEnabled = true
            self.companynameTf.isUserInteractionEnabled = true
            self.addresslineTf.isUserInteractionEnabled = true
            self.housenumberTf.isUserInteractionEnabled = true
            self.cityTf.isUserInteractionEnabled = true
            self.zipTf.isUserInteractionEnabled = true
            self.countryTf.isUserInteractionEnabled = true
            self.stateTf.isUserInteractionEnabled = true
            
            exixtingaddBtn.tag = 0
            self.useexistadd = "no"
            
        }
        
    }
    
    @IBAction func billingnextBtnClicked(_ sender: Any) {
        
        if billing == "no" {
        

            print("fulladd=\(fullnameTf.text ?? "nodata")")
            
//            if fullnameTf.text == "" || fullnameTf.text == nil {
//                self.showSimpleAlert(messagess: "Enter full name")
//            }else if companynameTf.text == nil || companynameTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter company name")
//            } else if addresslineTf.text == nil || addresslineTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter address line")
//            }else if housenumberTf.text == nil || housenumberTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter house number")
//            }else if cityTf.text == nil || cityTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter city")
//            }else if zipTf.text == nil || zipTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter zip code")
//            }else{
//
//                self.dataset()
//
//            }
            
//            if fullnameTf.text == "" || fullnameTf.text == nil {
//                self.showSimpleAlert(messagess: "Enter full name")
//            }else if housenumberTf.text == nil || housenumberTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter house number")
//            }else if addresslineTf.text == nil || addresslineTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter address line")
//            }else if cityTf.text == nil || cityTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter city")
//            }else if zipTf.text == nil || zipTf.text == "" {
//                self.showSimpleAlert(messagess: "Enter zip code")
//            }else{
                
                
                if fullnameTf.text == "" || fullnameTf.text == nil {
                    self.showSimpleAlert(messagess: "Enter full name")
                }else if addresslineTf.text == nil || addresslineTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter address line")
                }else if cityTf.text == nil || cityTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter city")
                }else if zipTf.text == nil || zipTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter zip code")
                    
                    
                }else if countryTf.text == nil || countryTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter country")
                    
                    
                }else if stateTf.text == nil || stateTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter state")
                    
                    
                }else{
                    
                
                self.dataset()
                
            }
            
            
        }else{
            
            if self.useexistadd == "no" {
                
                print("fulladd=\(fullnameTf.text ?? "nodata")")
                
//                if fullnameTf.text == "" || fullnameTf.text == nil {
//                    self.showSimpleAlert(messagess: "Enter full name")
//                }else if companynameTf.text == nil || companynameTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter company name")
//                } else if addresslineTf.text == nil || addresslineTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter address line")
//                }else if housenumberTf.text == nil || housenumberTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter house number")
//                }else if cityTf.text == nil || cityTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter city")
//                }else if zipTf.text == nil || zipTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter zip code")
//                }else{
//
//                    self.dataset()
//
//                }
                
                
//                if fullnameTf.text == "" || fullnameTf.text == nil {
//                    self.showSimpleAlert(messagess: "Enter full name")
//                }else if housenumberTf.text == nil || housenumberTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter house number")
//                }else if addresslineTf.text == nil || addresslineTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter address line")
//                }else if cityTf.text == nil || cityTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter city")
//                }else if zipTf.text == nil || zipTf.text == "" {
//                    self.showSimpleAlert(messagess: "Enter zip code")
//                }else{
                
                if fullnameTf.text == "" || fullnameTf.text == nil {
                    self.showSimpleAlert(messagess: "Enter full name")
                }else if addresslineTf.text == nil || addresslineTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter address line")
                }else if cityTf.text == nil || cityTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter city")
                }else if zipTf.text == nil || zipTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter zip code")
                }else if countryTf.text == nil || countryTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter country")
                    
                    
                }else if stateTf.text == nil || stateTf.text == "" {
                    self.showSimpleAlert(messagess: "Enter state")
                    
                    
                }else{
                    
                    self.dataset()
                    
                }
                
                
            }else{
               
                self.performSegue(withIdentifier: "paymentpage", sender: self)
                    
                }

                
            }
            

        let defaults = UserDefaults.standard
        defaults.set(passbillingdata, forKey: "billingaddressDICT")
           
        }
        
        
    func dataset()  {
        
        if self.billing == "no" {
            
            self.nameadd = fullnameTf.text! as NSString
            self.addressadd = addresslineTf.text! as NSString
            self.hounseadd = housenumberTf.text! as NSString
            self.cityadd = cityTf.text! as NSString
            self.stateadd = stateTf.text! as NSString
            if countryTf.text == "India" {
                self.countryadd = "IN"
            }else{
                
                self.countryadd = "US"
            }
            
            self.sipadd = zipTf.text! as NSString
            self.companynameadd = companynameTf.text! as NSString
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                        self.addBillingAddress()
            
        }else{
            
            if self.useexistadd == "no" {
                
                self.nameadd = fullnameTf.text! as NSString
                self.addressadd = addresslineTf.text! as NSString
                self.hounseadd = housenumberTf.text! as NSString
                self.cityadd = cityTf.text! as NSString
                self.stateadd = stateTf.text! as NSString
                if countryTf.text == "India" {
                    self.countryadd = "IN"
                }else{
                    
                    self.countryadd = "US"
                }
                
                self.sipadd = zipTf.text! as NSString
                self.companynameadd = companynameTf.text! as NSString
                
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.updateBillingAddress()
                
            }else{
                
                
            }
            
            
        }
        
        
    }
        
  
    func addBillingAddress()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalObjects.DevlopmentApi+"billing/"
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]
        

        AF.request(urlString, method: .post, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("add billing responce - \(dict)")
                                
                                
                                self.passbillingdata = dict
                                
                                let defaults = UserDefaults.standard
                                defaults.set(self.passbillingdata, forKey: "billingaddressDICT")

                                ERProgressHud.sharedInstance.hide()
                               
                                self.performSegue(withIdentifier: "paymentpage", sender: self)
                                
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                                self.showSimpleAlert(messagess:"Slow Internet Detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
        }

        
        
    }
    
    func updateBillingAddress()  {
        
        let  idd = Int(billId as String)
        
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerId = defaults.integer(forKey: "custId")
        let customeridStr = String(customerId)
        
        let avlCartId = defaults.object(forKey: "AvlbCartId")as! Int
        let cartidStr = String(avlCartId)
        
        let urlString = GlobalObjects.DevlopmentApi+"billing/\(idd ?? 0)/"
        
        print("update query - \(urlString)")
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho,
                "user_id": customeridStr,
                "cart_id": cartidStr,
                "action": "billing"
            ]
        

        AF.request(urlString, method: .put, parameters: ["name":self.nameadd,"company_name":self.companynameadd,"address":self.addressadd,"house_number":self.hounseadd,"zip":self.sipadd,"city":self.cityadd,"country":self.countryadd,"state":self.stateadd,"customer_id":"\(customerId)","priority":1],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 200{
                             
                             let dict :NSDictionary = response.value! as! NSDictionary
                              print("update billing responce - \(dict)")
                                
                                ERProgressHud.sharedInstance.hide()

                                self.passbillingdata = dict
                                let defaults = UserDefaults.standard
                                defaults.set(self.passbillingdata, forKey: "billingaddressDICT")
                               
                                ERProgressHud.sharedInstance.hide()
                                
                                self.performSegue(withIdentifier: "paymentpage", sender: self)
                               
                            }else{
                                
                                if response.response?.statusCode == 401{
                                    
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else  if response.response?.statusCode == 500{
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                    self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    
                                }
                                
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()
                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            
                            if error.localizedDescription == msg {
                                
                                self.showSimpleAlert(messagess:"No internet connection")
                                
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                                self.showSimpleAlert(messagess:"Slow Internet Detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
        }

        
        
    }
    
    
    func loadshippingMethod(){
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
        let storeId = defaults.object(forKey: "clickedStoreId")as? String
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
        let urlString = GlobalObjects.DevlopmentApi + "shipping-method/?status=ACTIVE&restaurant_id=\(GlobalObjects.restaurantGlobalid)"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

             AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                 
                                  
                                    
                                 let dict1 :NSDictionary = response.value! as! NSDictionary
                                    
                                    print("shipping Method - \(dict1)")
                                  
                                self.shippingMethod = (dict1.value(forKey:"results")as! NSArray)
                                    
                                    print("shipping Method result - \(self.shippingMethod)")
                                    
                                      if self.shippingMethod.count == 0 {
                                       
                                        ERProgressHud.sharedInstance.hide()

//                                        self.viewhide()
//                                        self.shippingmethodfullview.isHidden = true
//                                        self.storeselectLbl.isHidden = true
//                                        self.homeselctLbl.isHidden = true
                                        
                                        self.showSimpleAlert(messagess: "No shipping method available")
                                        
                                     
                                 }else{
                                       
                                    let dictObj = self.shippingMethod[0] as! NSDictionary
                                       
                                    let status = dictObj["name"]
                                    let statusid = dictObj["id"]
                                    
                                    defaults.set(status, forKey: "clickedShippingMethod")
                                    defaults.set(statusid, forKey: "clickedShippingMethodId")
                        
                                    ERProgressHud.sharedInstance.hide()

                                 }
                                     
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    @IBAction func statebutnClicked(_ sender: Any) {
        PageCount = 1
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        StateDict = []
        loadState()
        
        stateTable.register(UITableViewCell.self, forCellReuseIdentifier: "MyCell")

        blurview.isHidden = false
        
    }
    
    @IBAction func blurcancelClicked(_ sender: Any) {
        
        blurview.isHidden = true
        
    }
    
    
    @IBAction func caBTNClicked(_ sender: Any) {
        
        blurview.isHidden = true
        stateTf.text = "CA"
        
    }
    
    
    @IBAction func txBtnClicked(_ sender: Any) {
        
        blurview.isHidden = true
        stateTf.text = "TX"
    }
    
}
