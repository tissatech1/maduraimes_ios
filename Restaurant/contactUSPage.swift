//
//  contactUSPage.swift
//  Restaurant
//
//  Created by TISSA Technology on 12/8/20.
//

import UIKit
import Alamofire
import MessageUI

class contactUSPage: UIViewController,MFMessageComposeViewControllerDelegate, UINavigationControllerDelegate, MFMailComposeViewControllerDelegate{
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        self.dismiss(animated: true, completion: nil)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        dismiss(animated: true)
    }
    
    @IBOutlet weak var phTF: UILabel!
    @IBOutlet weak var emailTF: UILabel!
    @IBOutlet weak var addressTF: UILabel!
    @IBOutlet weak var timeTF: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        GlobalObjects.pageback = "yes"
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        getrestaurantApi()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(contactUSPage.tapFunction))
        phTF.isUserInteractionEnabled = true
        phTF.addGestureRecognizer(tap)
        
        let tap1 = UITapGestureRecognizer(target: self, action: #selector(contactUSPage.emailFunction))
        emailTF.isUserInteractionEnabled = true
        emailTF.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(contactUSPage.addressFunction))
        addressTF.isUserInteractionEnabled = true
        addressTF.addGestureRecognizer(tap2)
    }
   
    @IBAction func tapFunction(sender: UITapGestureRecognizer) {
            print("tap working")
        
        let stringddt = phTF.text!
        let url:NSURL = URL(string:"TEL://\(stringddt)")! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
        }
    
    @IBAction func emailFunction(sender: UITapGestureRecognizer) {
            print("email working")
        
        let emailddt = emailTF.text!
        let mc = MFMailComposeViewController()
        mc.mailComposeDelegate = self
        if MFMailComposeViewController.canSendMail() {
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = self as? MFMailComposeViewControllerDelegate
        mail.setToRecipients([emailddt])
        mail.setMessageBody("", isHTML: true)
        present(mail, animated: true)
        } else {
        print("Cannot send email")
        }
        
        
        
    }
    
//    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
//    controller.dismiss(animated: true)
//    }
  
//    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:NSError?) {
//        switch result.rawValue {
//        case MFMailComposeResultCancelled.rawValue:
//            print("Mail cancelled")
//        case MFMailComposeResultSaved.rawValue:
//            print("Mail saved")
//        case MFMailComposeResultSent.rawValue:
//            print("Mail sent")
//        case MFMailComposeResultFailed.rawValue:
//            print("Mail sent failure: %@", [error.localizedDescription])
//        default:
//            break
//        }
//        self.dismissViewControllerAnimated(true, completion: nil)
//    }
   
    
    @IBAction func addressFunction(sender: UITapGestureRecognizer) {
            print("address working")
        
      //  UIApplication.shared.openURL(URL(string:"https://www.google.com/maps/dir//Madurai+Mes+-+Authentic+Indian+cuisine+5152+Fredericksburg+Rd+San+Antonio,+TX+78229/@29.5025921,-98.5590801,16z/data=!4m5!4m4!1m0!1m2!1m1!1s0x865c5e1a91fca8a5:0x1f27b44a1998ff5b")!)
        
        let mapstr = "https://www.google.com/maps/dir//Madurai+Mes+-+Authentic+Indian+cuisine+5152+Fredericksburg+Rd+San+Antonio,+TX+78229/@29.5025921,-98.5590801,16z/data=!4m5!4m4!1m0!1m2!1m1!1s0x865c5e1a91fca8a5:0x1f27b44a1998ff5b"
        let url:NSURL = URL(string: mapstr)! as NSURL
        UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
        
    }
    
    
    @IBAction func contactback(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

       
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }
  
    
    
    func getrestaurantApi(){
        
        var admintoken = String()
        let defaults = UserDefaults.standard
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            admintoken = (defaults.object(forKey: "adminToken")as? String)!
        }else{
            admintoken = (defaults.object(forKey: "custToken")as? String)!
        }
        
        let autho = "token \(admintoken)"
        
        let urlString = GlobalObjects.DevlopmentApi+"restaurant/\(GlobalObjects.restaurantGlobalid)/"

           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                    
                                    let dict :NSDictionary = response.value! as! NSDictionary
                                   
                                   let dataPH  = dict["phone"]as? String
                                    let dataEmail  = dict["email"]as? String
                                    let dataADD  = dict["address"]as! String
                                    let dataCITY  = dict["city"]as! String
                                    let dataState  = dict["state"]as! String
                                    let dataZip  = dict["zip"]as! String
                                    let dataTime  = dict["working_hours"]as? String

                                    self.phTF.text! = dataPH ?? "(210)5248161"
                                    self.emailTF.text! = dataEmail ?? "maduraimes@gmail.com"
                                    self.addressTF.text! = "\(dataADD) \(dataCITY), \(dataState) \(dataZip)"
                                    
                                    self.timeTF.text! = dataTime ?? "Monday - Friday : 9 am to 5 pm Saturday and Sunday : Closed"
                                    
                                    ERProgressHud.sharedInstance.hide()

                                    
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                    self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                       

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                      

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
    
    
    
    
    
}
