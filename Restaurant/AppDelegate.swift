//
//  AppDelegate.swift
//  Restaurant
//
//  Created by TISSA Technology on 10/30/20.
//

import UIKit
import IQKeyboardManagerSwift


@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow? {
      didSet {
        window?.overrideUserInterfaceStyle = .light
      }
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        
        
        NSSetUncaughtExceptionHandler { (exception) in
           let stack = exception.callStackReturnAddresses
           print("Stack trace: \(stack)")

            }
        
//        let token = UserDefaults.standard.object(forKey: "Usertype")
//            if token == nil {
//                
//                
//                let storyboard = UIStoryboard(name: "Main", bundle: nil)
//                    let loginViewController: LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//
//                     var navigationController = UINavigationController()
//                     navigationController = UINavigationController(rootViewController: loginViewController)
//
//                     window?.rootViewController = navigationController
//
//                     //Navigation bar is hidden
//                     navigationController.isNavigationBarHidden = true
//                
//                
//            }
        
        
        return true
    }

    


}

