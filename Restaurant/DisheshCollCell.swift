//
//  DisheshCollCell.swift
//  Restaurant
//
//  Created by TISSA Technology on 12/4/20.
//

import UIKit

class DisheshCollCell: UICollectionViewCell {
    @IBOutlet weak var dishImage: UIImageView!
    @IBOutlet weak var dishname: UILabel!
    @IBOutlet weak var dishprice: UILabel!
    @IBOutlet weak var specialmenuouter: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
