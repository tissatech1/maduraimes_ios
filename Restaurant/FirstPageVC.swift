//
//  FirstPageVC.swift
//  Restaurant
//
//  Created by TISSA Technology on 12/4/20.
//

import UIKit
import SDWebImage
import Alamofire
import BNMenuViewController
import SimpleCollapsingHeaderView
import ProgressHUD



class FirstPageVC: UIViewController,UICollectionViewDelegate, UICollectionViewDataSource,UITableViewDataSource, UITableViewDelegate,BNMenuDelegate,UITextFieldDelegate,SimpleCollapsingHeaderViewDelegate {
    
    @IBOutlet weak var categoryCollView: UICollectionView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var searchsectionView: UIView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchcloseBtn: UIButton!
    @IBOutlet var headerView: SimpleCollapsingHeaderView!
    @IBOutlet weak var sidemenubutton: UIButton!
    @IBOutlet weak var topimg: UIImageView!
    @IBOutlet weak var cartcountView: UIView!
    @IBOutlet weak var cartcountLbl: UILabel!
    @IBOutlet weak var timingLbl: UILabel!
    @IBOutlet weak var categoryshNameLbl: UILabel!
    @IBOutlet weak var menulistTop: NSLayoutConstraint!
    @IBOutlet weak var resultLbl: UILabel!
    
    var categorywithdataDict = NSMutableDictionary()
    var categorywithdataArray = NSArray()
    var TitlesList = NSArray()
    var passedrestaurantid = Int()
    var passedcategoryid = Int()
    var passcategoryname = String()
    var getdataarray = NSArray()
    var menulist = NSMutableArray()
    var Specialmenulist = NSArray()
    var whichclicked = String()
    var srhPageLength = 0
    var whichdata = String()
    var scroll = String()
    var fetchedproduct = NSArray()
    var catiddlist = Array<Int>()
    var dataKeys : Array = [String]()
    var listshow = String()
    var didselect = String()
    var pagenumber = 1
    var lazyloading = String()
    var restStatusStr = String()
    var PageCount = Int()
    var isLoading = false
    
    func loadData() {
        isLoading = false
       // ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        GetMenuList()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.timingLbl.isHidden = true
        PageCount = 1
        GlobalObjects.pageback = "no"
        lazyloading = "yes"
        listshow = "withoutcat"
        didselect = "no"
        
     //   ERProgressHud.sharedInstance.show(withTitle: "Loading...")

       // GetSpecialMenuList()
         ProgressHUD.show("Loading...")

        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
        
        adminlogincheck()
            
        }else{
           
        checkCartAvl()
       // gettiming()
            self.GetCategoryList()
        }
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)

        let globalresto = GlobalObjects.restaurantGlobalid
        
        self.passedrestaurantid = Int(globalresto) ?? 1
        self.passedcategoryid = 1
        
        searchcloseBtn.isHidden = true
        
        cartcountView.layer.cornerRadius = 6
        cartcountView.layer.borderWidth = 2
        cartcountView.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        cartcountLbl.text = "0"
        
        let viewNib = UINib(nibName: "categorywithImageCell", bundle: nil)
        categoryCollView.register(viewNib, forCellWithReuseIdentifier: "cell")
        
        menuTableView.register(UINib(nibName: "menuCells", bundle: nil), forCellReuseIdentifier: "Cell")
        
        searchView.layer.cornerRadius = 8
        searchView.layer.shadowColor = UIColor.lightGray.cgColor
        searchView.layer.shadowOpacity = 1
        searchView.layer.shadowOffset = .zero
        searchView.layer.shadowRadius = 1
      
        let restid = GlobalObjects.restaurantGlobalid
        let defaults = UserDefaults.standard
        
        defaults.set(restid, forKey: "clickedStoreId")
        
        
        
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//
//        if GlobalObjects.pageback == "no" {
//
//            cartcountApi()
//
//        }else{
//
//         GlobalObjects.pageback = "no"
//
//            lazyloading = "yes"
//            listshow = "withoutcat"
//            didselect = "no"
//
//        ProgressHUD.show("Loading...")
//
//        GetCategoryList()
//
//        cartcountApi()
//
//        }
//
//    }
    
    
    
    
    @objc func backhomerefresh() {
     
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
      //  GetCategoryList()
        checkCartAvl()
        
    }
   
    
    func onHeaderDidAnimate(with percentage: CGFloat) {
        topimg.alpha = percentage
         sidemenubutton.alpha = percentage
        timingLbl.alpha = percentage
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
           
            headerView?.collapseHeaderView(using: menuTableView)
  
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
  
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
        adminlogincheckgettime()
        cartcountApi()
    }
    
    //MARK: - CollectionView Delegate Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       // return 2
        return 1
    }
    
     //** Number of Cells in the CollectionView */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       
//            if section == 0 {
//                return 1
//            }else{
            
        return TitlesList.count
                
         //   }
        }
    
    
    
    //** Create a basic CollectionView Cell */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! categorywithImageCell
        
            cell.ImageLable.layer.borderWidth = 1
            cell.ImageLable.layer.masksToBounds = true
            cell.ImageLable.layer.borderColor = UIColor.black.cgColor
            cell.ImageLable.layer.cornerRadius = cell.ImageLable.frame.height/2 //This will change with corners of image and height/2 will make this circle shape
            cell.ImageLable.clipsToBounds = true
        
            if TitlesList.count == 0 {

            }else{
             
//                if indexPath.section == 0 {
//
//                    cell.itemsLable.text = "Special Menu"
//                    cell.ImageLable.image = UIImage(named: "special_menu.jpg")
//
//                }else{
                
        let dictObj = self.TitlesList[indexPath.row] as! NSDictionary
        let category = dictObj["category"]

        cell.itemsLable.text = (category as! String)
            
          //  cell.itemsLable.text = self.array[indexPath.row]
                
               // cell.itemsLable.text = "test"
                
                
                var urlStr = String()
                if dictObj["category_url"] is NSNull || dictObj["category_url"] == nil{

                    urlStr = ""

                }else{
                    urlStr = dictObj["category_url"] as! String
                }
              
                let url = URL(string: urlStr )


                cell.ImageLable.sd_imageIndicator = SDWebImageActivityIndicator.gray
                cell.ImageLable.sd_setImage(with: url) { (image, error, cache, urls) in
                    if (error != nil) {
                        // Failed to load image
                        cell.ImageLable.image = UIImage(named: "noimage.png")
                    } else {
                        // Successful in loading image
                        cell.ImageLable.image = image
                    }
                }
                
             //   }
                
                
                
                
                
                

            }
            return cell
            
       
      
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {


            let cellSize = CGSize(width: 140, height: 120)
            return cellSize

       

    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
               lazyloading = "no"
        
//            if indexPath.section == 0 {
//
//                searchcloseBtn.isHidden = true
//                searchTF.text = ""
//                searchTF.placeholder = "Search our menu"
//
//                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
//                listshow = "withcat"
//                didselect = "yes"
////                categorywithdataDict = NSMutableDictionary()
////                dataKeys = []
//               // GetMenuList()
//                passcategoryname = "Special Menu"
//                categoryshNameLbl.text = "Special Menu"
//                self.menulist = []
//                isLoading = false
//                PageCount = 1
//                GetSpecialMenuList()
//
//
//            }else{
        
        let dictObj = self.TitlesList[indexPath.item] as! NSDictionary
       
        let restaurantid = dictObj["restaurant_id"]as! Int
        
        let restid = String(restaurantid)
        let defaults = UserDefaults.standard
        
        defaults.set(restid, forKey: "clickedStoreId")
        
            let categoryid = dictObj["category_id"]as! Int
                passedcategoryid = categoryid
                passedrestaurantid = restaurantid
                categoryshNameLbl.isHidden = false
                let categoryname  = dictObj["category"]as! String
                passcategoryname = categoryname
                categoryshNameLbl.text = passcategoryname
            searchcloseBtn.isHidden = true
            searchTF.text = ""
            searchTF.placeholder = "Search our menu"
            
            ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            listshow = "withcat"
            didselect = "yes"
            categorywithdataDict = NSMutableDictionary()
            dataKeys = []
                self.menulist = []
                isLoading = false
                PageCount = 1
            GetMenuList()
                
        //    }
            
       
//        let currentCell = categoryCollView.cellForItem(at: indexPath) as! categorywithImageCell
//
//        currentCell.backview.backgroundColor = .purple
  
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
//        let currentCell = categoryCollView.cellForItem(at: indexPath) as! categorywithImageCell
//
//        currentCell.backview.backgroundColor = .clear
    
    }
   
    //MARK: - Table View Delegates And Datasource
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        print(menulist.count)
        let lastData = self.menulist.count - 1
        if !isLoading && indexPath.row == lastData {
            PageCount += 1
            self.loadData()
        }
    }

    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if whichdata == "search" {
        return fetchedproduct.count
        }else{
        return menulist.count
        }
       
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! menuCells
        
        cell.selectionStyle = .none
       
    //    let dictObj = self.menulist[indexPath.row] as! NSDictionary


        if whichdata == "search" {

            if fetchedproduct.count == 0 {
                
            }else{
            let dictObj = self.fetchedproduct[indexPath.row] as! NSDictionary

            print(dictObj)
            
            
            var urlStr = String()
            if dictObj["productUrl"] is NSNull || dictObj["productUrl"] == nil{

                urlStr = ""

            }else{
                urlStr = dictObj["productUrl"] as! String
            }


            let rupee = "$ "
            
//            let pricedata = dictObj["price"]as! Double
//            let conprice = String(pricedata)
            
            var conprice = String()
            
            if let pricedata = dictObj["price"] as? String {
                 conprice = pricedata
            }else if let pricedata = dictObj["price"] as? NSNumber {
                
                 conprice = pricedata.stringValue
            }
        
            cell.dishprice.text = rupee + conprice
            
            cell.dishname.text!  = dictObj["productName"] as! String

            let url = URL(string: urlStr )

            cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
            cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
                if (error != nil) {
                    // Failed to load image
                    cell.dishimage.image = UIImage(named: "noimage.png")
                } else {
                    // Successful in loading image
                    cell.dishimage.image = image
                }
            }

            }
            }else{

                if menulist.count == 0 {
                    
                }else{

                let dictObj = self.menulist[indexPath.row] as! NSDictionary
                
                
        var urlStr = String()
        if dictObj["product_url"] is NSNull || dictObj["product_url"] == nil{

            urlStr = ""

        }else{
            urlStr = dictObj["product_url"] as! String
        }


        let rupee = "$"

         //   let pricedata = dictObj["price"]
               
                var conprice = String()
                
                if let pricedata = dictObj["price"] as? String {
                     conprice = pricedata
                }else if let pricedata = dictObj["price"] as? NSNumber {
                    
                     conprice = pricedata.stringValue
                }
                
             //   let conprice = pricedata

                cell.dishprice.text = rupee + conprice


        cell.dishname.text!  = dictObj["product_name"] as! String
                
        cell.dishdiscribe.text!  = dictObj["extra"] as! String

        let url = URL(string: urlStr )


        cell.dishimage.sd_imageIndicator = SDWebImageActivityIndicator.gray
        cell.dishimage.sd_setImage(with: url) { (image, error, cache, urls) in
            if (error != nil) {
                // Failed to load image
                cell.dishimage.image = UIImage(named: "noimage.png")
            } else {
                // Successful in loading image
                cell.dishimage.image = image
            }
        }
                }

        }

        return cell
    }
    

    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat{
        
        return 66
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     
        if restStatusStr == "open" {
            
            whichclicked = "tab"
            self.performSegue(withIdentifier: "MenuDetail", sender: self)
            
        }else{
            
            
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "MenuDetail" {
            let view = segue.destination as! MenuDetailPage

            if whichclicked == "tab" {
           
            if let itemIndex = menuTableView.indexPathForSelectedRow {
//
//                let sec = dataKeys[itemIndex.section]
//
//                let list:NSArray = categorywithdataDict.value(forKey: sec) as! NSArray
//                let dictObj = list[itemIndex.row] as! NSDictionary
//
//                print(dictObj)
                
                
             //   let dictObj = self.menulist[itemIndex.row] as! NSDictionary
 
                if whichdata == "search" {
                    
                    let dictObj = self.fetchedproduct[itemIndex.row] as! NSDictionary

                    let reqStr = dictObj["optional"]as! Bool
                    print(reqStr)
                    view.mustrequired = reqStr
                    
                    view.passdescription = dictObj["extra"]as! String
                    
                    let pidd = dictObj["productId"]as! String
                    view.passproductid = Int(pidd)!
                    view.passproductaName = (dictObj["productName"] as? String)!

                        if dictObj["productUrl"] is NSNull {
                            view.paasprodyctimage = ""
                        }else{
                    view.paasprodyctimage = (dictObj["productUrl"] as? String)!
                        }

                    let proprice = dictObj["price"] as! NSNumber

                    view.passunitprice = proprice.stringValue
                    
                    let restobj = dictObj["restaurant"] as! NSDictionary
                    
                    let restaurantid = restobj["restaurantId"]as! String
                    
                    let restid = restaurantid
                    let defaults = UserDefaults.standard
                    
                    defaults.set(restid, forKey: "clickedStoreId")
                    

                }else{
                    
                    let dictObj = self.menulist[itemIndex.row] as! NSDictionary

                    let reqStr = dictObj["optional"]as! Bool
                    print(reqStr)
                    view.mustrequired = reqStr
                    view.passdescription = dictObj["extra"]as! String
                view.passproductid = dictObj["product_id"] as! Int
                view.passproductaName = (dictObj["product_name"] as? String)!
                    
                    if dictObj["product_url"] is NSNull {
                        view.paasprodyctimage = ""
                    }else{
                view.paasprodyctimage = (dictObj["product_url"] as? String)!
                    }
                view.passunitprice = (dictObj["price"] as? String)!
                    
                    let restaurantid = dictObj["restaurant"]as! Int
                    
                    let restid = String(restaurantid)
                    let defaults = UserDefaults.standard
                    
                    defaults.set(restid, forKey: "clickedStoreId")
                    
                }
            }
            }else{
        

                
            }
            
        }else{
            
            
            
        }
    }
    

    //MARK: Webservice Call category
        
        
        func GetCategoryList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
           
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
                
            }else{
                
                admintoken = (defaults.object(forKey: "custToken")as? String)!
                
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalObjects.DevlopmentApi+"category/?restaurant_id=\(GlobalObjects.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                     
                                     let dict :NSArray = response.value! as! NSArray
                                     
                                        if dict.count == 0 {
                                            self.categoryshNameLbl.isHidden = false
                                            self.TitlesList = []
                                            self.categoryCollView.reloadData()
                                            
                                        }else{
                                            
                                            
                                          
                                            self.TitlesList = dict
                                            
                    let dictObjcat = self.TitlesList[0] as! NSDictionary
                    let categoryiddd = dictObjcat["category_id"]
                    self.passedcategoryid = categoryiddd as! Int
                    self.passcategoryname = dictObjcat["category"]as! String
                    self.categoryshNameLbl.isHidden = false
                    self.categoryshNameLbl.text = self.passcategoryname
                               
                                            self.GetMenuList()
                                            
                      
                                            print("catidlist - \(self.catiddlist)")

                                            self.categoryCollView.reloadData()
                                            
                                        }
                                        
                                      //  ERProgressHud.sharedInstance.hide()

                                     
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
        
      
    
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        ProgressHUD.dismiss()

                                        
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
    
    func SessionAlert() {
        let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        
                                        ProgressHUD.dismiss()

                                        //Sign out action
                                      
                                        UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                        UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                        UserDefaults.standard.removeObject(forKey: "custToken")
                                        UserDefaults.standard.removeObject(forKey: "custId")
                                    UserDefaults.standard.removeObject(forKey: "Usertype")
                                    UserDefaults.standard.synchronize()
                                        
                                        self.performSegue(withIdentifier: "backlogin", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
    
    
    @IBAction func menuBtnClicked(_ sender: UIButton) {
        
        let modalController = BNMenuViewController()
        modalController.modalPresentationStyle = .overCurrentContext
        modalController.delegate = self
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
            modalController.arrayList = ["Login", "About Us", "Contact Us"]
            modalController.arrayIcons = ["enter.png", "info.png", "email.png"]
            
        }else{
        let defaults = UserDefaults.standard
        let userlog = defaults.object(forKey: "Userlog")as! String
        
        if userlog == "reguser" {
        
        modalController.arrayList = ["Profile", "My Orders", "About Us", "Contact Us", "Logout"]
        modalController.arrayIcons = ["man.png", "my_order.png", "info.png", "email.png", "logout.png"]
            
        }else{
            
            modalController.arrayList = [ "My Orders", "About Us", "Contact Us", "Logout"]
            modalController.arrayIcons = ["my_order.png", "info.png", "email.png", "logout.png"]
        }
            
        }
            
        modalController.iconSize = CGSize(width: 45, height: 45)
        modalController.itemTextColor = .black
        modalController.itemTextFont = UIFont(name: "Baskerville-SemiBoldItalic", size: 20)!
        present(modalController, animated: false, completion: nil)
       
    }
    
    func logout(){
        
        let defaults = UserDefaults.standard
        let admintoken = defaults.object(forKey: "custToken")as? String
        let customerid = defaults.integer(forKey: "custId")
        let customeridStr = String(customerid)
        print("customeridStr = \(customeridStr)")
        print("customer token = \(String(describing: admintoken))")
        print("global customer token = \(GlobalObjects.customertoken)")

        
        let autho = "token \(GlobalObjects.customertoken)"
        
        let urlString = GlobalObjects.DevlopmentApi+"rest-auth/logout/"
        
           
        print(" categoryurl - \(urlString)")
        
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]
      

        AF.request(urlString, method: .post, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
            response in
              switch response.result {
                            case .success:
                                print(response)

                                if response.response?.statusCode == 200{
                                 
                               //  let dict :NSDictionary = response.value! as! NSDictionary
                                 
                                    print(response)
                                    self.performSegue(withIdentifier: "backlogin", sender: self)

                                    ERProgressHud.sharedInstance.hide()

                                 
                                }else{
                                    
                  if response.response?.statusCode == 401{
                                    
                    ERProgressHud.sharedInstance.hide()

                   // self.SessionAlert()
                          
                                    
                                    }
                                    
                                    
                                    if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {
                                    
                            self.showSimpleAlert(messagess:"No internet connection")
                                    
                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                    
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
        // MARK: - BNMenuDelegate
        func menuSelectedAtIndex(i: Int) {
            print(i)
            
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                
                if i == 0 {
                    GlobalObjects.backlogTag = "first"
                    self.performSegue(withIdentifier: "backlogin", sender: self)
                    
                    }else if i == 1 {
                        
                        self.performSegue(withIdentifier: "aboutUS", sender: self)
               
                     }else if i == 2 {
                        
                        self.performSegue(withIdentifier: "contactus", sender: self)
                        
                     }
                
            }else{
            
            let defaults = UserDefaults.standard
            let userlog = defaults.object(forKey: "Userlog")as! String
            
            if userlog == "reguser" {
            
            if i == 0 {
           
                    self.performSegue(withIdentifier: "profilepage", sender: self)
           
                }else if i == 1 {
                    
                    self.performSegue(withIdentifier: "OrderpageVC", sender: self)
           
                 }else if i == 2 {
                    
                    self.performSegue(withIdentifier: "aboutUS", sender: self)
                    
                 }else if i == 3 {
                    self.performSegue(withIdentifier: "contactus", sender: self)

           
                 }else if i == 4 {
           
                   Afterlogouthomerefresh()
           
                 }
                
            }else{
                
                if i == 0 {
               
                        self.performSegue(withIdentifier: "OrderpageVC", sender: self)
               
                    }else if i == 1 {
                        
                        self.performSegue(withIdentifier: "aboutUS", sender: self)
               
                     }else if i == 2 {
                        
                        self.performSegue(withIdentifier: "contactus", sender: self)
                        
                     }else if i == 3 {
                        
                        Afterlogouthomerefresh()
                        
                     }
            }
                
            }
            
        }
    
         func Afterlogouthomerefresh() {
            let alert = UIAlertController(title: nil, message: "Are you sure you want to logout?",         preferredStyle: UIAlertController.Style.alert)
    
            alert.addAction(UIAlertAction(title: "YES", style: UIAlertAction.Style.default, handler: { _ in
    
                    UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                    UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                    UserDefaults.standard.removeObject(forKey: "custToken")
                    UserDefaults.standard.removeObject(forKey: "custId")
                UserDefaults.standard.removeObject(forKey: "Usertype")
                UserDefaults.standard.synchronize()
    
                ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                self.logout()
    
            }))
            alert.addAction(UIAlertAction(title: "NO",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            //Sign out action
    
    
            }))
    
            if presentedViewController == nil {
                self.present(alert, animated: true, completion: nil)
            } else{
                self.dismiss(animated: false) { () -> Void in
                    self.present(alert, animated: true, completion: nil)
                  }
            }
    
         //   present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor(rgb: 0xFE9300)
    
    
        }
   
    //MARK: check cart Section
    
    //MARK: check cart Api
       
        func checkCartAvl()  {
           
           let defaults = UserDefaults.standard
           
           let savedUserData = defaults.object(forKey: "custToken")as? String
           
           let customerid = defaults.integer(forKey: "custId")
           let custidStr = String(customerid)
           
           let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
               
            let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id="+custidStr+"&restaurant=\(GlobalObjects.restaurantGlobalid)"
           print("Url cust avl - \(urlString)")
               
               let headers: HTTPHeaders = [
                   "Content-Type": "application/json",
                   "Authorization": token,
                   "user_id": custidStr,
                   "action": "cart"
               ]

                AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
               response in
                 switch response.result {
                               case .success:
                                   
                                 //  print(response)

                                   if response.response?.statusCode == 200{
                                     //  self.dissmiss()
                                       
                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                    //  print(dict)
                                       
                                       let status = dict.value(forKey: "results")as! NSArray
                                                      print(status)
                                                        
                                           print("store available or not - \(status)")
                                       
                                       
                                                        
                   if status.count == 0 {
                       
                  //  ERProgressHud.sharedInstance.show(withTitle: "Loading...")
                       self.createCart()
                                                           
                   }else{
                            
                               let firstobj:NSDictionary  = status.object(at: 0) as! NSDictionary

                               let getAvbcartId = firstobj["id"] as! Int
                        let storeWRTCart = firstobj["restaurant"] as! Int


                        let defaults = UserDefaults.standard

                               defaults.set(getAvbcartId, forKey: "AvlbCartId")
                               defaults.set(storeWRTCart, forKey: "storeIdWRTCart")


                                    print("avl cart id - \(getAvbcartId)")
                                    print("cart w R to store  - \(storeWRTCart)")


                    self.cartcountApi()
                       
                 //   ERProgressHud.sharedInstance.hide()

               }
                     
               }else{
                                       
                  // self.dissmiss()
                   print(response)
                   if response.response?.statusCode == 401{
                                          
                       self.SessionAlert()
                                           
                    }else if response.response?.statusCode == 500{
                                                                      
                 //  self.dissmiss()
                                                                      
                                                                       let dict :NSDictionary = response.value! as! NSDictionary
                                                                      
                                                                      self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                                                  }else{
                                                                   
                                                                   self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                                  }
               }
                                   
                                   break
                               case .failure(let error):
                                ERProgressHud.sharedInstance.hide()

                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                
                                let msgrs = "URLSessionTask failed with error: The request timed out."
                                
                                if error.localizedDescription == msg {

                            self.showSimpleAlert(messagess:"No internet connection")

                        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                            self.showSimpleAlert(messagess:"Slow Internet Detected")

                                }else{
                                
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    
                                }

                                   print(error)
                            }
               }
               
         
               
           }
       
       
    
    func createCart() {
        
        
        let defaults = UserDefaults.standard
        
        let savedUserData = defaults.object(forKey: "custToken")as? String
         let token = "Token \(savedUserData ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
       
        let customerid = defaults.integer(forKey: "custId")
        let custidStr = String(customerid)
       

//        let urlString = GlobalObjects.DevlopmentApi+"cart/?customer_id=\(customerid)&restaurant_id=\(passedrestaurantid)"

        let urlString = GlobalObjects.DevlopmentApi+"cart/"
        
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": token,
            "user_id": custidStr,
            "action": "cart"
        ]
        
        AF.request(urlString, method: .post, parameters: ["customer_id":customerid,"restaurant":GlobalObjects.restaurantGlobalid],encoding: JSONEncoding.default, headers: headers).responseJSON {
        response in
          switch response.result {
                        case .success:
                            print(response)

                            if response.response?.statusCode == 201{
                                                 //   self.dissmiss()
                                                    
                                                    let resultarr :NSDictionary = response.value! as! NSDictionary
                                
                                                print(resultarr)
                                
                                
                             //   let resultarr : NSArray = dict.value(forKey: "results") as! NSArray
                                
                                if resultarr.count == 0 {
                                  
                                    self.showSimpleAlert(messagess: "Cart not created")
                                    
                                }else{
                                
                             //   let dictObj = resultarr[0] as! NSDictionary

                                                    
                                                    let getAvbcartId = resultarr["id"] as! Int
                                                                                                      let storeWRTCart = resultarr["restaurant"] as! Int
                                                                                                      
                                                                                                      let defaults = UserDefaults.standard
                                                                                                      
                                                                                                      defaults.set(getAvbcartId, forKey: "AvlbCartId")
                                                                                                      defaults.set(storeWRTCart, forKey: "storeIdWRTCart")
                                                                                                           
                                                                                                           print("avl cart id - \(getAvbcartId)")
                                                                                                           print("cart w R to store  - \(storeWRTCart)")
                                                  
                                    self.cartcountApi()
                                    
                            //    self.dissmiss()
                                  
                       //     self.performSegue(withIdentifier: "productListVC", sender: self)
                                    
                                }
                                
                            //    ERProgressHud.sharedInstance.hide()

                                
                            }else{
                             
                              if response.response?.statusCode == 500{
                                                            
                                   //                         self.dissmiss()
                                                            
                                                             let dict :NSDictionary = response.value! as! NSDictionary
                                                       
                                let message  = dict["message"]as! String
                           let substr = "duplicate key value violates unique constraint"
                                
                                if message.contains(substr) {
                                    
                                    print("I found: \(message)")
                                }else{
                                    
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    
                                }
                                                        }else{
                                                            
                                                      //      self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                                           }
                            }
                            
                            break
                        case .failure(let error):
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow Internet Detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
        }


     }
    
    
    //MARK: Webservice Call Menu
        
        
        func GetMenuList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            let autho = "token \(admintoken)"
            
            var urlString = String()

            
            urlString = GlobalObjects.DevlopmentApi+"catalog/?restaurant_id=\(passedrestaurantid)&category_id=\(passedcategoryid)&status=ACTIVE&page=\(PageCount)"
                
           
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        self.getdataarray = []
                                     
                                     let dict :NSDictionary = response.value! as! NSDictionary
                                     
                                        let list:NSArray = dict.value(forKey: "results") as! NSArray
                                        self.getdataarray = list
                                        
                                        print(dict)
                                        
                                        
                                        if list.count == 0 {
                                           
                                            self.whichdata = "list"
                                            self.menulist = []
                                            self.menuTableView.reloadData()

                                            ERProgressHud.sharedInstance.hide()
                                          
                                          
                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            
                                            
                                        }else{
                                            
                                            self.whichdata = "list"
                                            
                                            self.menulist.addObjects(from: self.getdataarray as! [Any])
                 
//                                            self.menulist = list as! NSMutableArray
                                          
                                            
                                            self.menuTableView.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()
                                            
                                           
                                        }
                                        
                                        ProgressHUD.dismiss()

                                     
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
    
    
    //MARK: Webservice Call SpecialMenu
        
        
        func GetSpecialMenuList(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalObjects.DevlopmentApi+"specialmenu/?restaurant_id=\(GlobalObjects.restaurantGlobalid)&page=\(PageCount)"
            
               
            print(" category clicked menu api - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                   // "user_id": customeridStr,
                  //  "action": "specialmenu"
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                   // print(response)

                                    if response.response?.statusCode == 200{
                                   
                                        self.getdataarray = []
                                     
                                    
                             let list :NSArray = response.value! as! NSArray
                                        self.getdataarray = list
                                        print(list)
                                        
                                        if list.count == 0 {
                                            self.menulist = []
                                            self.menuTableView.reloadData()

                                        self.showSimpleAlert(messagess: "No menu available in this category")
                                            
                                        }else{
                                            
                                            self.whichdata = "list"
                                            
                                            self.menulist.addObjects(from: self.getdataarray as! [Any])
                   
                                            self.menuTableView.reloadData()
                                            
                                            ERProgressHud.sharedInstance.hide()
                                            
                                           
                                        }
                                        
                                        
                                        ERProgressHud.sharedInstance.hide()

                                     
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {
                                        
                                self.showSimpleAlert(messagess:"No internet connection")
                                        
                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                        
                                self.showSimpleAlert(messagess:"Slow internet detected")
                                        
                                    }else{
                                    
                                        self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                }
                }
                
          
                
            }
    //MARK: Webservice Call Search product  by productname
    
    func productSearchCall() {
        
        let searchdata = searchTF.text!
        let extra = ""
        let restaurantid = GlobalObjects.restaurantGlobalid
       
        let page = Int(srhPageLength)

        let urlString = GlobalObjects.DevlopmentGraphql
            

    //    let stringPassed = "query{productSearch(token:\"\(GlobalObjects.globGraphQlToken)\", productName :\"\(searchdata)\", restaurantId : \(restaurantid) , extra : \"\(extra)\" , first:100, skip:\(page)){\n productId\n productName\n   productUrl\n  price\n extra\n taxExempt\n category\n {\n categoryId \n category \n } \n restaurant { \n restaurantId \n address \n} } \n}"
        
        let stringPassed = "query{productSearch(token:\"\(GlobalObjects.globGraphQlToken)\", productName :\"\(searchdata)\", restaurantId : \(restaurantid) , extra : \"\(extra)\" , first:100, skip:\(page)){\n productId\n productName\n   productUrl\n  price\n extra\n optional\n taxExempt\n category\n {\n categoryId \n category \n } \n restaurant { \n restaurantId \n address \n} } \n}"
        
        
       
        
        print("pro search w textUrl- \(stringPassed)")

            AF.request(urlString, method: .post, parameters: ["query": stringPassed],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
             case .success:
               //  print(response)

                 if response.response?.statusCode == 200{
                
                let dict :NSDictionary = response.value! as! NSDictionary
                let status = dict.value(forKey: "data")as! NSDictionary
                print(status)
                    let productsearchArr :NSArray = status.value(forKey: "productSearch")as! NSArray
                    
                   
                
                    if productsearchArr.count == 0 {
                        self.categoryCollView.reloadData()
//                        self.categorywithdataDict = NSMutableDictionary()
//                        self.dataKeys = []
                        self.fetchedproduct = []
                        
                        self.menulist = []
                        self.categoryshNameLbl.isHidden = true
                       // self.showSimpleAlert(messagess:"No menu found for given search")
                        self.whichdata = "search"
                        self.menuTableView.reloadData()
                        ERProgressHud.sharedInstance.hide()
                    }else{
                        
                        self.categoryCollView.reloadData()
                       
                        self.whichdata = "search"
                        
                        let adddishes = NSMutableArray()

                        adddishes.addObjects(from: self.fetchedproduct as! [Any])
                      
                     //   print("pro search w text - \(adddishes)")
                        
                        self.fetchedproduct = productsearchArr
                        
                        self.menulist = []
                        self.categoryshNameLbl.isHidden = true
                       
                        self.menuTableView.reloadData()
                        ERProgressHud.sharedInstance.hide()
                    }
                  
                 }else{
                    
                    if response.response?.statusCode == 401{
                    
                        ERProgressHud.sharedInstance.hide()
                  self.SessionAlert()
                  
                    }else if response.response?.statusCode == 500{
                        
                        ERProgressHud.sharedInstance.hide()

                        let dict :NSDictionary = response.value! as! NSDictionary
                        
                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                    }else if response.response?.statusCode == 404{
                       
                        ERProgressHud.sharedInstance.hide()
                      //  self.fetchedproduct = []
                        
                        
                    }else{
                        
                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                       }
                    
                    
                    
                 }
                 
                 break
             case .failure(let error):
                
                ERProgressHud.sharedInstance.hide()

                print(error.localizedDescription)
                
                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                
                let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                
                let msgrs = "URLSessionTask failed with error: The request timed out."
                
                if error.localizedDescription == msg {
                    
            self.showSimpleAlert(messagess:"No internet connection")
                    
        }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                    
            self.showSimpleAlert(messagess:"Slow internet detected")
                    
                }else{
                
                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                }

                   print(error)
            }
           }


        }
    
    //MARK: - Textfield Delegate

    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        searchcloseBtn.isHidden = false
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTF {
          
            lazyloading = "no"
            
            srhPageLength = 0
            self.scroll = "no"
            self.menulist = []
            searchcloseBtn.isHidden = false
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
            self.menulist = []
            categorywithdataDict = NSMutableDictionary()
            dataKeys = []
            didselect = "no"
        productSearchCall()
            
        textField.resignFirstResponder()
        }

        return true
    }
    
    @IBAction func searchcloseClicked(_ sender: Any) {
        self.menulist = []
        isLoading = false
        PageCount = 1
        categoryshNameLbl.isHidden = false
        searchcloseBtn.isHidden = true
        searchTF.text = ""
        searchTF.placeholder = "Search our menu"
        didselect = "no"
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        listshow = "withoutcat"
        self.menulist = []
        categorywithdataDict = NSMutableDictionary()
        dataKeys = []
       // GetMenuList()
        GetCategoryList()
        
    }
    
    @IBAction func specialmenuClicked(_ sender: Any) {
        
        self.performSegue(withIdentifier: "specialmenu", sender: self)

    }
   
    
    func cartcountApi()
   {

      let defaults = UserDefaults.standard
      let customerid = defaults.integer(forKey: "custId")

        //  let customerid = 16
        
    let urlString = GlobalObjects.DevlopmentGraphql

        let restidd = GlobalObjects.restaurantGlobalid

    let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid),restaurantId:\(restidd)){\n count\n }\n}"

    //    let stringempty = "query{cartItemCount(token:\"\(GlobalObjects.globGraphQlToken)\", customerId :\(customerid)){\n count\n }\n}"
        
        
            AF.request(urlString, method: .post, parameters: ["query": stringempty],encoding: JSONEncoding.default, headers: nil).responseJSON {
           response in
             switch response.result {
                           case .success:
                              // print(response)

                               if response.response?.statusCode == 200{

                              let dict :NSDictionary = response.value! as! NSDictionary
                             // print(dict)
                                
                                if dict.count == 2 {

                                   // self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                    self.cartcountLbl.text = "0"
                                    
                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }else{
                                
                                
                                
                                let status = dict.value(forKey: "data")as! NSDictionary
                              print(status)


                                let newdict = status.value(forKey: "cartItemCount")as! NSDictionary

                                let num = newdict["count"] as! Int

                                self.cartcountLbl.text = String(num)

                                    let countStr = self.cartcountLbl.text
                                    let defaults = UserDefaults.standard
                                    defaults.set(countStr, forKey: "cartdatacount")
                                    
                                }
                              //  self.dissmiss()

                               }else{

                                
                                if response.response?.statusCode == 500{
                                    
                              //      ERProgressHud.sharedInstance.hide()

                              //      let dict :NSDictionary = response.value! as! NSDictionary
                                    
                                 //   self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                }else if response.response?.statusCode == 401{
                                    ERProgressHud.sharedInstance.hide()
                                    self.SessionAlert()
                                    
                                }else{
                                    
                                    self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                   }
                                


                               }

                               break
                           case .failure(let error):
                            
                            ERProgressHud.sharedInstance.hide()

                            print(error.localizedDescription)
                            
                            let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                            
                            let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                            
                            let msgrs = "URLSessionTask failed with error: The request timed out."
                            
                            if error.localizedDescription == msg {
                                
                        self.showSimpleAlert(messagess:"No internet connection")
                                
                    }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{
                                
                        self.showSimpleAlert(messagess:"Slow internet detected")
                                
                            }else{
                            
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                            }

                               print(error)
                        }
           }


        }

    
    @IBAction func cartBtnClicked(_ sender: Any) {
        
        let token = UserDefaults.standard.object(forKey: "Usertype")
        if token == nil {
            
        }else{
        
        if restStatusStr == "open" {
        
        self.performSegue(withIdentifier: "CartpageVC", sender: self)
            
        }else{
            
            
        }
        }
        
    }
   
    
    
    //MARK: Webservice Call timing
        
        
        func gettiming(){
            
            var admintoken = String()
            let defaults = UserDefaults.standard
            let token = UserDefaults.standard.object(forKey: "Usertype")
            if token == nil {
                admintoken = (defaults.object(forKey: "adminToken")as? String)!
            }else{
                admintoken = (defaults.object(forKey: "custToken")as? String)!
            }
            
            let autho = "token \(admintoken)"
            
            let urlString = GlobalObjects.DevlopmentApi+"hour/?restaurant_id=\(GlobalObjects.restaurantGlobalid)"
            
               
            print(" categoryurl - \(urlString)")
            
                let headers: HTTPHeaders = [
                    "Content-Type": "application/json",
                    "Authorization": autho
                ]
          

            AF.request(urlString, method: .get, parameters: nil,encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                  switch response.result {
                                case .success:
                                    print(response)

                                    if response.response?.statusCode == 200{
                                        
                                        let dict :NSDictionary = response.value! as! NSDictionary
                                       
                                        let data  = dict["status"]as! String
                                        
                                        self.restStatusStr = data
                                        
                                      //  self.restStatusStr = "open"
                                        
                                       // data = "closed"
                                        
                                        if data == "closed" {
                                         
                                            print("restaurant is - \(data)")
                                            
                                          //  self.categoryshNameLbl.text = "Closed"
                                            self.timingLbl.isHidden = false
                                            self.menuTableView.alpha = 0.5
                                            self.categoryCollView.alpha = 0.5
                                            
                                        }else{
                                        
                                            self.timingLbl.isHidden = true
                                            self.menuTableView.alpha = 1
                                            self.categoryCollView.alpha = 1
                                            print("restaurant is - open")
                                        }
                                        
                                      //  self.GetCategoryList()
                                        
                                    }else{
                                        
                      if response.response?.statusCode == 401{
                                        
                        ERProgressHud.sharedInstance.hide()

                        self.SessionAlert()
                              
                                        
                                        }
                                        
                                        
                                        if response.response?.statusCode == 500{
                                            
                                            ERProgressHud.sharedInstance.hide()

                                            let dict :NSDictionary = response.value! as! NSDictionary
                                            
                                            self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                        }
                                        
                                        
                                        
                                    }
                                    
                                    break
                                case .failure(let error):
                                    ERProgressHud.sharedInstance.hide()

                                    print(error.localizedDescription)
                                    
                                    let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                    
                                    let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                                    
                                    let msgrs = "URLSessionTask failed with error: The request timed out."
                                    
                                    if error.localizedDescription == msg {

                                self.showSimpleAlert(messagess:"No internet connection")

                            }else if error.localizedDescription == msgr || error.localizedDescription == msgrs{

                                self.showSimpleAlert(messagess:"Slow Internet Detected")

                                    }else{
                                    
                                self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                    }

                                       print(error)
                                    
                                    
                                    
                                }
                }
                
          
                
            }
        
   
    //MARK: Admin Login
    
    func adminlogincheck(){

        let urlString = GlobalObjects.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalObjects.adminusername, "password":GlobalObjects.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                           // self.gettiming()
                            self.GetCategoryList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    
    
    func adminlogincheckgettime(){

        let urlString = GlobalObjects.DevlopmentApi + "rest-auth/login/v1/"

        AF.request(urlString, method: .post, parameters: ["username":GlobalObjects.adminusername, "password":GlobalObjects.adminpassword,"restaurant_id":"1"],encoding: JSONEncoding.default, headers: nil).responseJSON {
       response in
         switch response.result {
                       case .success:
                           print(response)

                           if response.response?.statusCode == 200{
                            
                            let dict :NSDictionary = response.value! as! NSDictionary
                            // print(dict)
                            
                            let tok = dict.value(forKey: "token")
                            
                            let defaults = UserDefaults.standard
                            
                            defaults.set(tok, forKey: "adminToken")
                            self.gettiming()
                          //  self.GetCategoryList()
                          
                           }else{
                            
                            if response.response?.statusCode == 401{
                                
                                ERProgressHud.sharedInstance.hide()
                               // self.sessionAlert()
                                
                            }else if response.response?.statusCode == 500{
                                
                                ERProgressHud.sharedInstance.hide()

                                 let dict :NSDictionary = response.value! as! NSDictionary
                                
                                self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                
                               //  print(dict.value(forKey: "msg") as! String)
                            }else{
                                
                                self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                               }
                          
                           }
                           
                           break
                       case .failure(let error):
                        
                        ERProgressHud.sharedInstance.hide()
                        print(error.localizedDescription)
                        
                        let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                        
                        
                        let msgr = "URLSessionTask failed with error: A server with the specified hostname could not be found."
                        
                        let msgrs = "URLSessionTask failed with error: The request timed out."
                        
                        
                        if error.localizedDescription == msg {
                            
                            self.showSimpleAlert(messagess:"No internet connection")
                            
                        }else if error.localizedDescription == msgr ||  error.localizedDescription == msgrs {
                            
                            self.showSimpleAlert(messagess:"Slow Internet Detected")
                                    
                                }else{
                        
                            self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                        }

                           print(error)
                       }
       }


    }
    
    
    
}


