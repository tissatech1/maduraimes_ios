//
//  changePassVC.swift
//  Restaurant
//
//  Created by TISSA Technology on 12/14/20.
//

import UIKit
import Alamofire

class changePassVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    @IBOutlet weak var view3: UIView!
    @IBOutlet weak var oldpassTf: UITextField!
    @IBOutlet weak var newpassTf: UITextField!
    @IBOutlet weak var confirmPasstf: UITextField!
    @IBOutlet weak var submitBtn: UIButton!

    var usernameget = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        submitBtn.layer.cornerRadius = 6
        
        view1.layer.cornerRadius = 6
        view1.layer.borderWidth = 1
        view1.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        view2.layer.cornerRadius = 6
        view2.layer.borderWidth = 1
        view2.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor
        
        view3.layer.cornerRadius = 6
        view3.layer.borderWidth = 1
        view3.layer.borderColor = UIColor(rgb: 0xFE9300).cgColor

        
        
    }
    
    
    

    @IBAction func changepaaBack(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: false)

       
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {

        
        textField.isSecureTextEntry = true
            
    }

    func changepasswordApi()  {
        
        let defaults = UserDefaults.standard
        
     //   let admintoken = defaults.object(forKey: "adminToken")as? String
        let admintoken = defaults.object(forKey: "custToken")as? String
       // let customerId = defaults.integer(forKey: "custId")
        
      
        
        let autho = "token \(admintoken ?? "243d3a495d1f076806eebbd1b2335ada59de9e05")"
        
    let urlString = GlobalObjects.DevlopmentApi + "/rest-auth/password/reset/confirm/v1/"
        
        print("category Url -\(urlString)")
            
            let headers: HTTPHeaders = [
                "Content-Type": "application/json",
                "Authorization": autho
            ]

        AF.request(urlString, method: .post, parameters: ["username":usernameget, "old_password":oldpassTf.text!,"new_password1":newpassTf.text!,"new_password2":confirmPasstf.text!],encoding: JSONEncoding.default, headers: headers).responseJSON { [self]
            response in
              switch response.result {
                            case .success:
                               // print(response)

                                if response.response?.statusCode == 200{
                                
                                    ERProgressHud.sharedInstance.hide()
                                    let alert = UIAlertController(title:nil , message: "Password changed successfully",         preferredStyle: UIAlertController.Style.alert)

                                  
                                    alert.addAction(UIAlertAction(title: "OK",
                                                                  style: UIAlertAction.Style.default,
                                                                  handler: {(_: UIAlertAction!) in
                                self.navigationController?.popViewController(animated: true)

                                                                    
                                                                    
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                    alert.view.tintColor = UIColor(rgb: 0x199A48)
                                    
                                 
                                }else{
                                    
                                    if response.response?.statusCode == 401{
                                    
                                        ERProgressHud.sharedInstance.hide()

                                        self.SessionAlert()
                                        
                                    }else if response.response?.statusCode == 500{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else if response.response?.statusCode == 400{
                                        
                                        ERProgressHud.sharedInstance.hide()

                                        let dict :NSDictionary = response.value! as! NSDictionary
                                        
                                        self.showSimpleAlert(messagess: dict.value(forKey: "msg") as! String)
                                    }else{
                                        
                                        self.showSimpleAlert(messagess: "The system is temporarily unavailable. Please try again later")
                                       }
                                    
                                    
                                    
                                    
                                }
                                
                                break
                            case .failure(let error):

                                ERProgressHud.sharedInstance.hide()
                                print(error.localizedDescription)
                                
                                let msg = "URLSessionTask failed with error: The Internet connection appears to be offline."
                                
                                
                                if error.localizedDescription == msg {
                                    
                                    self.showSimpleAlert(messagess:"No internet connection")
                                    
                                }else{
                                
                                    self.showSimpleAlert(messagess:"\(error.localizedDescription)")
                                }

                                   print(error)
                            }
            }
            
      
            
        }
    
    
@IBAction func changepasssubmit(_ sender: UIButton) {
 
    if oldpassTf.text == "Current password" || oldpassTf.text == ""{
        showSimpleAlert(messagess: "Enter current password")
    }else if newpassTf.text == "New password" || newpassTf.text == ""{
        
        showSimpleAlert(messagess: "Enter new password")
    }else if newpassTf.text!.count <= 8{
        
        showSimpleAlert(messagess: "Password must be greater than 8 characters")
    }else if confirmPasstf.text == "Confirm password" || confirmPasstf.text == ""{
        
        showSimpleAlert(messagess: "Enter confirm password")
    }else if confirmPasstf.text! != newpassTf.text!{
        
        showSimpleAlert(messagess: "New password and confirm password does not match")
    }else{
        
        ERProgressHud.sharedInstance.show(withTitle: "Loading...")
        changepasswordApi()
        
    }
    
    
    
    }
    
   
    func showSimpleAlert(messagess : String) {
        let alert = UIAlertController(title: "", message: messagess,         preferredStyle: UIAlertController.Style.alert)

      
        alert.addAction(UIAlertAction(title: "OK",
                                      style: UIAlertAction.Style.default,
                                      handler: {(_: UIAlertAction!) in
                                        ERProgressHud.sharedInstance.hide()
                                        //Sign out action
        }))
        self.present(alert, animated: true, completion: nil)
        alert.view.tintColor = UIColor(rgb: 0xFE9300)
    }
        
        func SessionAlert() {
            let alert = UIAlertController(title: "Session Expired", message: "Please login again.",         preferredStyle: UIAlertController.Style.alert)

          
            alert.addAction(UIAlertAction(title: "OK",
                                          style: UIAlertAction.Style.default,
                                          handler: {(_: UIAlertAction!) in
                                            ERProgressHud.sharedInstance.hide()
                                            //Sign out action
                                          
                                            UserDefaults.standard.removeObject(forKey: "AvlbCartId")
                                            UserDefaults.standard.removeObject(forKey: "storeIdWRTCart")
                                            UserDefaults.standard.removeObject(forKey: "custToken")
                                            UserDefaults.standard.removeObject(forKey: "custId")
                                        UserDefaults.standard.removeObject(forKey: "Usertype")
                                        UserDefaults.standard.synchronize()
                                            
                                            self.performSegue(withIdentifier: "backlogin", sender: self)
            }))
            self.present(alert, animated: true, completion: nil)
            alert.view.tintColor = UIColor(rgb: 0xFE9300)
        }
        
        
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.view.alpha = 0.7
        UIView.animate(withDuration: 1.5, animations: {
                self.view.alpha = 1.0
            })
    }

}
